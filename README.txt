Hello, this is a quick tutotial on how to get this application to work on your computer.

I will be assuming you have IntelliJ installed and that you are using it.

First open up the frontend and the backend in IntelliJ. Those both should be configured project. Run NPM install and maven import to get all the dependencys sorted out.

Then you want to add a file to the directory */backend/src/main/resources whic is the application.properties file.

There you would want todefine your database, if you use MariaDB and you username defined there is root, as well as your password and you created a new database, which is called pnp_tables while using the port 3306 for your database, this would valid contents for that file:

spring.jpa.hibernate.ddl-auto=update
spring.datasource.url=jdbc:mysql://localhost:3306/pnp_tables_online?serverTimezone=UTC
spring.datasource.username=root
spring.datasource.password=root
spring.servlet.multipart.max-file-size=128MB
spring.servlet.multipart.max-request-size=128MB


Next you want to create a upload-dir directory located in */backend/upload-dir and fill it with any text file as a placeholder, so it is not automatically deleted.

Now you are ready to start up your own version of this little application, have fun!

If you have no user in the database yet, a admin-user is created for you to confirm user with and make your own admins. For security reasons you might want to delete him as soon as his purpose is served!
It's username is admin and the password is Th1s1sTheS1tuati0nTh1sP455word1sF0r.