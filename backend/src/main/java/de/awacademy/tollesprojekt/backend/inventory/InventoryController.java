package de.awacademy.tollesprojekt.backend.inventory;

import de.awacademy.tollesprojekt.backend.adventurer.Adventurer;
import de.awacademy.tollesprojekt.backend.adventurer.AdventurerService;
import de.awacademy.tollesprojekt.backend.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class InventoryController {

    private InventoryService inventoryService;
    private AdventurerService adventurerService;

    @Autowired
    public InventoryController(InventoryService inventoryService, AdventurerService adventurerService) {
        this.inventoryService = inventoryService;
        this.adventurerService = adventurerService;
    }

    /**
     *
     */
    @GetMapping("/api/inventory")
    public InventoryItemDTO[] getInventory(@ModelAttribute("sessionUser") User sessionUser,
                                           @RequestParam("owningAdv") String adventurerName,
                                           @RequestParam("owningUser") String username) {
        if (sessionUser == null) {
            return null;
        }
        List<InventoryItem> inventory = adventurerService
                .getAdventurerByAdventurerAndUserName(adventurerName, username)
                .getInventory();
        return inventoryService.getDTOArrayofInventoryItemsList(inventory);
    }

    /**
     *
     */
    @PostMapping("api/addItem")
    public void addNewInventoryItem(@ModelAttribute("sessionUser") User sessionUser,
                                    @RequestBody InventoryCreationDTO inventoryCreation) {
        if (sessionUser == null) {
            return;
        }
        if (!(inventoryCreation.getItem().getName().matches("^[a-zA-Z0-9äöüÄÖÜß ]+$")
                && (inventoryCreation.getItem().getName().length() >= 3)
                && (inventoryCreation.getItem().getName().length() <= 20)
                && inventoryCreation.getItem().getDescription().matches("^[a-zA-Z0-9äöüÄÖÜß.,!? ]+$")
                && (inventoryCreation.getItem().getDescription().length() >= 3)
                && (inventoryCreation.getItem().getDescription().length() <= 200))) {
            return;
        }
        InventoryItem item = new InventoryItem(inventoryCreation.getItem().getName(),
                inventoryCreation.getItem().getDescription(), inventoryCreation.getItem().getAmount(),
                adventurerService.getAdventurerByAdventurerAndUserName(inventoryCreation.getAdvName(), inventoryCreation.getUserName()));

        inventoryService.saveItemInInventory(item);
    }

    /**
     *
     */
    @PatchMapping("/api/changeAmountOfItem")
    public void changeAmountOfItem(@ModelAttribute("sessionUser") User sessionUser,
                                   @RequestBody InventoryCreationDTO inventoryCreation) {
        if (sessionUser == null) {
            return;
        }
        if (inventoryCreation == null) {
            return;
        }
        if (inventoryCreation.getItem() == null) {
            return;
        }
        InventoryItem item = inventoryService.getItemByItemNameAdventurerNameUserName(inventoryCreation.getItem().getName(),
                inventoryCreation.getAdvName(), inventoryCreation.getUserName());
        if (item == null) {
            return;
        }
        item.setAmount(inventoryCreation.getItem().getAmount());
        inventoryService.saveItemInInventory(item);
    }

    /**
     *
     */
    @DeleteMapping("/api/deleteItem")
    public void deleteInventoryItem(@ModelAttribute("sessionUser") User sessionUser,
                                    @RequestParam("itemName") String itemName,
                                    @RequestParam("adventurerName") String adventurerName,
                                    @RequestParam("userName") String userName) {
        if (sessionUser == null) {
            return;
        }
        Adventurer adventurer = adventurerService.getAdventurerByAdventurerAndUserName(adventurerName, userName);
        if (adventurer == null) {
            return;
        }
        if (!(adventurer.getUser() == sessionUser || sessionUser.isAdminStatus())) {
            return;
        }
        for (InventoryItem item : adventurer.getInventory()) {
            if (item.getName().equals(itemName)) {
                inventoryService.deleteItem(item);
                return;
            }

        }
    }


}
