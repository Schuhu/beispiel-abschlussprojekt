package de.awacademy.tollesprojekt.backend.user;

import de.awacademy.tollesprojekt.backend.adventurer.Adventurer;
import de.awacademy.tollesprojekt.backend.adventurer.AdventurerService;
import de.awacademy.tollesprojekt.backend.gameTable.GameTable;
import de.awacademy.tollesprojekt.backend.gameTable.GameTableService;
import de.awacademy.tollesprojekt.backend.inventory.InventoryItem;
import de.awacademy.tollesprojekt.backend.inventory.InventoryService;
import de.awacademy.tollesprojekt.backend.security.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

@RestController
public class UserController {
    private UserService userService;
    private AdventurerService adventurerService;
    private GameTableService gameTableService;
    private InventoryService inventoryService;

    @Autowired
    public UserController(UserService userService, AdventurerService adventurerService,
                          GameTableService gameTableService, InventoryService inventoryService) {
        this.userService = userService;
        this.adventurerService = adventurerService;
        this.gameTableService = gameTableService;
        this.inventoryService = inventoryService;
    }

    /**
     * This method is a controller-method and as such is supposed to be used by the frontend as a requestpoint of the backend.
     * This method is a simple request for a RegisterDTO
     * and can be used to see if someone has a login cookie by useing Springsecurity and the http-Basic-login.
     *
     * @return An empty RegisterDTO.
     */
    @GetMapping("/api/register")
    public RegisterDTO register() {
        return new RegisterDTO("", "", "");
    }

    /**
     * This method is a controller-method and as such is supposed to be used by the frontend as a requestpoint of the backend.
     * It is used to register a new User. The new User still has to be confirmed by and admin.
     *
     * @param registerDTO The information to register a new User.
     * @return returns the UserDTO for the user who made it to the database.
     * @throws UserAlreadyExistAuthenticationException A self-made exception thrown when the user already exists.
     */
    @PostMapping("/api/register")
    public UserDTO register(@RequestBody RegisterDTO registerDTO) throws UserAlreadyExistAuthenticationException {
        if (userService.checkIfUserNamePresent(registerDTO.getUsername())) {
            throw new UserAlreadyExistAuthenticationException("user already exists");
        }
        if (registerDTO.getUsername() == null
                || registerDTO.getPassword() == null
                || registerDTO.getRoleInGroup() == null) {
            return null;
        }
        if (!(registerDTO.getUsername().matches("^[a-zA-Z0-9äöüÄÖÜß ]+$")
                && (registerDTO.getUsername().length() >= 3)
                && (registerDTO.getUsername().length() <= 20))
                || !(registerDTO.getPassword().matches("^[a-zA-Z0-9äöüÄÖÜß]+$")
                && (registerDTO.getPassword().length() >= 4)
                && (registerDTO.getPassword().length() <= 20))
                || !(registerDTO.getRoleInGroup().equals("player")
                || registerDTO.getRoleInGroup().equals("gamemaster")
                || registerDTO.getRoleInGroup().equals("visitor"))) {
            return null;
        }
        User user = userService.registerUser(registerDTO);
        return new UserDTO(user.getUsername(), user.isAdminStatus(), user.getRoleInGroup());
        //ToDo: Get all controllers on this level, to get a nice code going.
    }

    /**
     * This method is a controller-method and as such is supposed to be used by the frontend as a requestpoint of the backend.
     * It is used to get an array of all confirmed Users from the database.
     *
     * @param sessionUser The sessionUser, can be inferred by the ControllerAdvice with @ModelAttribute("sessionUser").
     * @return The array to UserDTOs to be send to the frontend.
     */
    @GetMapping("/api/userList")
    public UserDTO[] getListofUsersConfirmed(@ModelAttribute("sessionUser") User sessionUser) {
        return getListofUsers(sessionUser, true);
    }

    /**
     * This method is a controller-method and as such is supposed to be used by the frontend as a requestpoint of the backend.
     * It is used to get an array of all unconfirmed Users from the database.
     *
     * @param sessionUser The sessionUser, can be inferred by the ControllerAdvice with @ModelAttribute("sessionUser").
     * @return The array to UserDTOs for unconfirmed Users to be send to the frontend.
     */
    @GetMapping("/api/userListUnconfirmed")
    public UserDTO[] getListofUsersUnconfirmed(@ModelAttribute("sessionUser") User sessionUser) {
        return getListofUsers(sessionUser, false);
    }

    /**
     * This method is used by getListOfUsersConfirmed and getListofUsersUnconfirmed, as the have many similarities.
     *
     * @param sessionUser     The sessionUser, can be inferred by the ControllerAdvice with @ModelAttribute("sessionUser").
     * @param listIsConfirmed True is you want to get confirmed Users, false is you want to get unconfirmed Users.
     * @return The array of the UserDTOs generated from the Users fitting the confirm-status.
     */
    private UserDTO[] getListofUsers(User sessionUser, boolean listIsConfirmed) {
        if (!sessionUser.isAdminStatus()) {
            return new UserDTO[0];
        }
        List<UserDTO> userDTOList = new LinkedList<>();
        List<User> userList = userService.getUsers();
        for (User user : userList) {
            if (!user.getUsername().equals(sessionUser.getUsername()) && (user.isConfirmed() == listIsConfirmed)) {
                userDTOList.add(new UserDTO(user.getUsername(), user.isAdminStatus(), user.getRoleInGroup()));
            }
        }
        return userDTOList.toArray(new UserDTO[0]);
    }

    /**
     * This method is a controller-method and as such is supposed to be used by the frontend as a requestpoint of the backend.
     * Is is used to grant someone the admin-status and take it away by switching it to the opposite one the DTO has.
     *
     * @param sessionUser The sessionUser, can be inferred by the ControllerAdvice with @ModelAttribute("sessionUser").
     * @param userDTO The UserDTO with the information of the user, which the admin-status is to be changed for.
     */
    @PatchMapping("/api/nominateAdmin")
    public void patchChangeAdminStatus(@ModelAttribute("sessionUser") User sessionUser,
                                       @RequestBody UserDTO userDTO) {
        if (!sessionUser.isAdminStatus()) return;
        User user = userService.getUserByUsername(userDTO.getUsername());
        user.setAdminStatus(!userDTO.isAdminStatus());
        userService.saveUser(user);
    }

    /**
     * This method is a controller-method and as such is supposed to be used by the frontend as a requestpoint of the backend.
     * It is used to switch the status for a user from unconfirmed to confirmed.
     *
     * @param sessionUser The sessionUser, can be inferred by the ControllerAdvice with @ModelAttribute("sessionUser").
     * @param userDTO The UserDTO with the information of the user, who is to be confirmed.
     */
    @PatchMapping("/api/confirmUser")
    public void confirmUser(@ModelAttribute("sessionUser") User sessionUser,
                            @RequestBody UserDTO userDTO) {
        if (!sessionUser.isAdminStatus()) return;
        User user = userService.getUserByUsername(userDTO.getUsername());
        user.setConfirmed(true);
        userService.saveUser(user);
    }

    /**
     * This method is a controller-method and as such is supposed to be used by the frontend as a requestpoint of the backend.
     * It is used to delete a User.
     *
     * @param sessionUser      The sessionUser, can be inferred by the ControllerAdvice with @ModelAttribute("sessionUser").
     * @param userToDeleteName The username of the User, who is to be deleted.
     */
    @DeleteMapping("/api/deleteUser")
    public void deleteUser(@ModelAttribute("sessionUser") User sessionUser,
                           @RequestParam("userToDelete") String userToDeleteName) {
        if (!sessionUser.isAdminStatus()) {
            return;
        }
        User userToDelete = userService.getUserByUsername(userToDeleteName);
        if (userToDelete.isAdminStatus()) {
            return;
        }
        for (Adventurer adventurer : userToDelete.getAdventurerList()) {
            for (InventoryItem inventoryItem : adventurer.getInventory()) {
                inventoryService.deleteItem(inventoryItem);
            }
            adventurerService.deleteAdventurer(adventurer);
        }
        for (GameTable gameTable : userToDelete.getGameTableList()) {
            gameTableService.deleteGameTable(gameTable);
        }
        userService.deleteUser(userToDelete);
    }
}
