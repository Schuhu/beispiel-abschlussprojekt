package de.awacademy.tollesprojekt.backend.adventurer;

import de.awacademy.tollesprojekt.backend.inventory.InventoryItem;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface AdventurerRepository extends CrudRepository<Adventurer, UUID> {

    List<Adventurer> findAllByOrderByNameAsc();
    Adventurer findAdventurerByInventory(InventoryItem inventoryItem);
}
