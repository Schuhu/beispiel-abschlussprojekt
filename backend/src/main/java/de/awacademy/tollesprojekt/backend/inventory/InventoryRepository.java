package de.awacademy.tollesprojekt.backend.inventory;

import org.springframework.data.repository.CrudRepository;

import java.util.UUID;


public interface InventoryRepository extends CrudRepository<InventoryItem, UUID> {

}
