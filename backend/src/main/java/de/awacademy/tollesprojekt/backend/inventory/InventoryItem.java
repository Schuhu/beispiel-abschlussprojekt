package de.awacademy.tollesprojekt.backend.inventory;

import de.awacademy.tollesprojekt.backend.adventurer.Adventurer;

import javax.persistence.*;
import java.util.UUID;

/**
 * This is the Entity class for the inventory items, Entitys represent the Database entries in Java.
 */

@Entity
public class InventoryItem {

    @Id
    @GeneratedValue
    @Column(columnDefinition = "BINARY(16)")
    private UUID id;

    private String name;
    private String description;
    private int amount;

    @ManyToOne
    private Adventurer adventurer;


    public InventoryItem(String name, String description, int amount, Adventurer adventurer) {
        this.name = name;
        this.description = description;
        this.amount = amount;
        this.adventurer = adventurer;
    }

    public InventoryItem() {
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getAmount() {
        return amount;
    }

    public Adventurer getAdventurer() {
        return adventurer;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
