package de.awacademy.tollesprojekt.backend.gameTable;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface GameTableRepository extends CrudRepository<GameTable, UUID> {
    List<GameTable> findAllByOrderByNameAsc();
    List<GameTable> findAllByNameContaining(String name);
}
