package de.awacademy.tollesprojekt.backend.user;

import de.awacademy.tollesprojekt.backend.adventurer.Adventurer;
import de.awacademy.tollesprojekt.backend.gameTable.GameTable;

import java.util.List;

public class OwnUserDTO {

    private String username;
    private boolean adminStatus;
    private String roleInGroup;
    private List<GameTable> gameTableList;
    private List<Adventurer> adventurerList;

    public static OwnUserDTO of(User user) {
        if (user == null){
            return new OwnUserDTO("");
        }
        return new OwnUserDTO(user.getUsername(), user.isAdminStatus(),
                user.getRoleInGroup(), user.getGameTableList(), user.getAdventurerList());
    }


    public OwnUserDTO(String username, boolean adminStatus, String roleInGroup, List<GameTable> gameTableList, List<Adventurer> adventurerList) {
        this.username = username;
        this.adminStatus = adminStatus;
        this.roleInGroup = roleInGroup;
        this.gameTableList = gameTableList;
        this.adventurerList = adventurerList;
    }

    public OwnUserDTO(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public boolean isAdminStatus() {
        return adminStatus;
    }

    public String getRoleInGroup() {
        return roleInGroup;
    }

    public List<GameTable> getGameTableList() {
        return gameTableList;
    }

    public List<Adventurer> getAdventurerList() {
        return adventurerList;
    }
}
