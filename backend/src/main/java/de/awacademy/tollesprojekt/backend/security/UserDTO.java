package de.awacademy.tollesprojekt.backend.security;

import java.util.UUID;

public class UserDTO {

    private String username;
    private boolean adminStatus;
    private String roleInGroup;

    public UserDTO(String username, boolean adminStatus, String roleInGroup) {
        this.username = username;
        this.adminStatus = adminStatus;
        this.roleInGroup = roleInGroup;
    }

    public UserDTO(){}

    public String getUsername() {
        return username;
    }

    public boolean isAdminStatus() {
        return adminStatus;
    }

    public String getRoleInGroup() {
        return roleInGroup;
    }

    public void setAdminStatus(boolean adminStatus) {
        this.adminStatus = adminStatus;
    }
}
