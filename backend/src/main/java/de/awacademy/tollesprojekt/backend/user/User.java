package de.awacademy.tollesprojekt.backend.user;

import de.awacademy.tollesprojekt.backend.adventurer.Adventurer;
import de.awacademy.tollesprojekt.backend.gameTable.GameTable;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * This is the Entity class for the users, Entitys represent the Database entries in Java.
 * What a user is should be self explanatory,
 * our users currently have the attributes username, password, adminstatus, roleingroup and confirmed.
 */

@Entity
public class User {

    @Id
    @GeneratedValue
    @Column(columnDefinition = "BINARY(16)")
    private UUID id;

    private String username;
    private String password;
    private boolean adminStatus;
    private String roleInGroup;
    private boolean confirmed;

    @OneToMany(mappedBy = "owningGM")
    private List<GameTable> gameTableList;

    @OneToMany(mappedBy = "user")
    private List<Adventurer> adventurerList;

    @ManyToMany(mappedBy = "subscribedUsers")
    private Set<GameTable> subscribedGameTables = new HashSet<>();

    @ManyToMany(mappedBy = "tempSubscribedUsers")
    private Set<GameTable> tempSubscribedGameTables = new HashSet<>();

    public User() {
    }

    public User(String username, String password, boolean adminStatus, String roleInGroup, boolean confirmed) {
        this.username = username;
        this.password = password;
        this.adminStatus = adminStatus;
        this.roleInGroup = roleInGroup;
        this.confirmed = confirmed;
    }

    public UUID getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public boolean isAdminStatus() {
        return adminStatus;
    }

    public String getRoleInGroup() {
        return roleInGroup;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public List<Adventurer> getAdventurerList() {
        return adventurerList;
    }

    public List<GameTable> getGameTableList() {
        return gameTableList;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAdminStatus(boolean adminStatus) {
        this.adminStatus = adminStatus;
    }

    public void setRoleInGroup(String roleInGroup) {
        this.roleInGroup = roleInGroup;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public Set<GameTable> getSubscribedGameTables() {
        return subscribedGameTables;
    }

    public Set<GameTable> getTempSubscribedGameTables() {
        return tempSubscribedGameTables;
    }
}
