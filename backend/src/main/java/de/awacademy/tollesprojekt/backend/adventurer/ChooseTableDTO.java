package de.awacademy.tollesprojekt.backend.adventurer;

public class ChooseTableDTO {

    private String sessionUserName;
    private String owningGMName;
    private String gametableName;
    private String adventurerName;

    public ChooseTableDTO(String sessionUserName, String owningGMName, String gametableName, String adventurerName) {
        this.sessionUserName = sessionUserName;
        this.owningGMName = owningGMName;
        this.gametableName = gametableName;
        this.adventurerName = adventurerName;
    }

    public String getSessionUserName() {
        return sessionUserName;
    }

    public String getOwningGMName() {
        return owningGMName;
    }

    public String getGametableName() {
        return gametableName;
    }

    public String getAdventurerName() {
        return adventurerName;
    }

}
