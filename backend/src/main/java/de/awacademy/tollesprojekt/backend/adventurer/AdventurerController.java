package de.awacademy.tollesprojekt.backend.adventurer;

import de.awacademy.tollesprojekt.backend.adventurer.storage.StorageService;
import de.awacademy.tollesprojekt.backend.gameTable.GameTable;
import de.awacademy.tollesprojekt.backend.inventory.InventoryItem;
import de.awacademy.tollesprojekt.backend.inventory.InventoryService;
import de.awacademy.tollesprojekt.backend.user.User;
import de.awacademy.tollesprojekt.backend.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
public class AdventurerController {

    private AdventurerService adventurerService;
    private UserService userService;
    private StorageService storageService;
    InventoryService inventoryService;

    @Autowired
    public AdventurerController(AdventurerService adventurerService,
                                StorageService storageService,
                                UserService userService,
                                InventoryService inventoryService) {
        this.adventurerService = adventurerService;
        this.storageService = storageService;
        this.userService = userService;
        this.inventoryService = inventoryService;
    }

    /**
     * This method is a controller-method and as such is supposed to be used by the frontend as a requestpoint of the backend.
     *
     * @param sessionUser    The sessionUser, can be inferred by the ControllerAdvice with @ModelAttribute("sessionUser").
     * @param file           A RequestParam with the mapping "file". The file to be uploaded.
     * @param userName       A RequestParam with the mapping "user". The user which tries to upload the file.
     * @param adventurerName A RequestParam with the mapping "adventurer". The adventurer the file is to be added to.
     * @return Return a ResponseEntity<String> with either a success or a failure message.
     * @throws IOException Signals that an I/O exception of some sort has occurred.
     */
    @PostMapping("/api/upload")
    public ResponseEntity<String> handleFileUpload(@ModelAttribute("sessionUser") User sessionUser,
                                                   @RequestParam("file") MultipartFile file,
                                                   @RequestParam("user") String userName,
                                                   @RequestParam("adventurer") String adventurerName) throws IOException {
        if (!file.getContentType().equals("application/pdf")) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("Wrong filetype");
        }
        if (!sessionUser.getRoleInGroup().equals("player")) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("Only a player may upload a PDF.");
        }
        String message = "";
        MultipartFile multipartFile = new MockMultipartFile(userName + "_" + adventurerName + "PDF" + ".pdf",
                userName + "_" + adventurerName + "PDF" + ".pdf", file.getContentType(), file.getInputStream());
        try {
            if (storageService.doesFileExist(adventurerName, userName)) {
                storageService.deleteFile(adventurerService.getAdventurerByAdventurerAndUserName(adventurerName, userName));
            }
            storageService.store(multipartFile);
            String pdfReference = adventurerService.createPdfReference(multipartFile);
            List<Adventurer> adventurerList = userService.getUserByUsername(userName).getAdventurerList();
            for (Adventurer adventurer : adventurerList) {
                if (adventurer.getName().equals(adventurerName)) {
                    adventurer.setPdfReference(pdfReference);
                    adventurerService.saveAdventurer(adventurer);
                    break;
                }
            }

            message = "You successfully uploaded" + multipartFile.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.OK).body(message);
        } catch (Exception e) {
            message = "Fail to upload" + file.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        }
    }

    /**
     * This method is a controller-method and as such is supposed to be used by the frontend as a requestpoint of the backend.
     *
     * @param username A RequestParam with the mapping "user". The username of the user you want to get the adventurers for.
     * @return All adventurers from the user with the specified username as AdventurerDTO NOT as Adventurers, use user.getAdventurerList for that.
     */
    @GetMapping("/api/adventurerListofUser")
    public AdventurerDTO[] getAdventurerListOfUser(@RequestParam("user") String username) {
        User user = userService.getUserByUsername(username);
        List<Adventurer> listOfAdventurer = user.getAdventurerList();
        return adventurerService.getDTOListOfAdventurer(listOfAdventurer).toArray(new AdventurerDTO[0]);
    }

    /**
     * This method is a controller-method and as such is supposed to be used by the frontend as a requestpoint of the backend.
     *
     * @param sessionUser The sessionUser, can be inferred by the ControllerAdvice with @ModelAttribute("sessionUser").
     * @return
     */
    @GetMapping("/api/adventurerList")
    public AdventurerDTO[] getAdventurerList(@ModelAttribute("sessionUser") User sessionUser) {
        if (!sessionUser.isAdminStatus()) return null;
        List<Adventurer> listOfAdventurer = adventurerService.getListOfAdventurers();
        return adventurerService.getDTOListOfAdventurer(listOfAdventurer).toArray(new AdventurerDTO[0]);
    }

    /**
     * This method is a controller-method and as such is supposed to be used by the frontend as a requestpoint of the backend.
     *
     * @param link
     * @return
     */
    @GetMapping("/api/getFile")
    @ResponseBody
    public ResponseEntity<Resource> getFile(@RequestParam("link") String link) {
        Resource file = storageService.loadFile(link);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .body(file);
    }

    /**
     * This method is a controller-method and as such is supposed to be used by the frontend as a requestpoint of the backend.
     *
     * @param sessionUser   The sessionUser, can be inferred by the ControllerAdvice with @ModelAttribute("sessionUser").
     * @param adventurerDTO The ResponseBody for this postmapping. The AdventurerDTO for an adventurer to be saved into the Database. The adventurer is saved with "" as it's PDF-Reference.
     */
    @PostMapping("/api/newAdventurer")
    public void postNewAdventurer(@ModelAttribute("sessionUser") User sessionUser,
                                  @RequestBody AdventurerDTO adventurerDTO) {
        if (!sessionUser.getRoleInGroup().equals("player")) return;
        if (!(adventurerDTO.getName().matches("^[a-zA-Z0-9äöüÄÖÜß ]+$")
                && (adventurerDTO.getName().length() >= 3)
                && (adventurerDTO.getName().length() <= 20))) {
            return;
        }
        User player = userService.getUserByUsername(adventurerDTO.getUser().getUsername());
        List<Adventurer> referenceList = player.getAdventurerList();
        for (Adventurer adventurer : referenceList) {
            if (adventurer.getName().equals(adventurerDTO.getName())) {
                return;
            }
        }
        Adventurer adventurer = new Adventurer(adventurerDTO.getName(), "",
                player);
        adventurerService.saveAdventurer(adventurer);
    }

    /**
     * This method is a controller-method and as such is supposed to be used by the frontend as a requestpoint of the backend.
     *
     * @param sessionUser    The sessionUser, can be inferred by the ControllerAdvice with @ModelAttribute("sessionUser").
     * @param chooseTableDTO The ResponseBody for this PatchMapping. The ChooseTableDTO contains all the variables to be able to assign the specified adventurer in it to the specified table in it.
     */
    @PatchMapping("/api/chooseTableForAdventurer")
    public void postSetTableForAdventurer(@ModelAttribute("sessionUser") User sessionUser,
                                          @RequestBody ChooseTableDTO chooseTableDTO) {

        List<Adventurer> adventurerList =
                userService.getUserByUsername(chooseTableDTO.getSessionUserName()).getAdventurerList();
        List<GameTable> gameTableList =
                userService.getUserByUsername(chooseTableDTO.getOwningGMName()).getGameTableList();
        for (Adventurer adventurer : adventurerList) {
            if (adventurer.getName().equals(chooseTableDTO.getAdventurerName())) {
                for (GameTable gameTable : gameTableList) {
                    if (gameTable.getName().equals(chooseTableDTO.getGametableName())) {
                        adventurer.setGameTable(gameTable);
                        adventurerService.saveAdventurer(adventurer);
                        return;
                    }
                }
            }
        }
    }

    /**
     * This method is a controller-method and as such is supposed to be used by the frontend as a requestpoint of the backend.
     *
     * @param sessionUser    The sessionUser, can be inferred by the ControllerAdvice with @ModelAttribute("sessionUser").
     * @param userName       A RequestParam with the mapping "userName". The username of the user the adventurer you want to delete belongs to.
     * @param adventurerName A RequestParam with the mapping "advenrurerName". The name of the adventurer you want to delete.
     * @throws IOException Signals that an I/O exception of some sort has occurred.
     */
    @DeleteMapping("/api/deleteAdventurer")
    public void deleteAdventurer(@ModelAttribute("sessionUser") User sessionUser,
                                 @RequestParam("userName") String userName,
                                 @RequestParam("adventurerName") String adventurerName) throws IOException {
        for (Adventurer adventurer : userService.getUserByUsername(userName).getAdventurerList()) {
            if (adventurer.getName().equals(adventurerName)) {
                if (storageService.doesFileExist(adventurer.getName(), adventurer.getUser().getUsername())) {
                    storageService.deleteFile(adventurer);
                }
                for (InventoryItem inventoryItem : adventurer.getInventory()) {
                    inventoryService.deleteItem(inventoryItem);
                }
                adventurerService.deleteAdventurer(adventurer);
                return;
            }
        }
    }


}
