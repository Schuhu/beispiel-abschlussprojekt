package de.awacademy.tollesprojekt.backend.adventurer.storage;

import de.awacademy.tollesprojekt.backend.adventurer.Adventurer;
import de.awacademy.tollesprojekt.backend.adventurer.AdventurerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


@Service
public class StorageService {

    Logger log = LoggerFactory.getLogger(this.getClass().getName());
    private final Path rootLocation = Paths.get("upload-dir");
    private AdventurerService adventurerService;

    @Autowired
    public StorageService(AdventurerService adventurerService) {
        this.adventurerService = adventurerService;
    }

    /**
     * A method to store a file. It is saved to upload.dir directory inside the src directory.
     * @param file The file you would like to store.
     */
    public void store(MultipartFile file) {
        try {
            Files.copy(file.getInputStream(), this.rootLocation.resolve(file.getOriginalFilename()));
        } catch (Exception e) {
            throw new RuntimeException("FAIL!");
        }
    }

    /**
     * A method to load up a file from a PDF-Reference (gained from the adventurer this pdf should represent).
     *
     * @param pdfReference Said PDF-Reference
     * @return Said file, it should be a PDF. (Should because there is no check for that in this method).
     */
    public Resource loadFile(String pdfReference) {
        try {
            Path file = rootLocation.resolve(pdfReference);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("FAIL!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("FAIL!");
        }
    }

    /**
     * Delete the PDF-file assigned to a specific adventurer.
     *
     * @param adventurer The adventurer you want to delete the file for.
     * @throws IOException Signals that an I/O exception of some sort has occurred.
     */
    public void deleteFile(Adventurer adventurer) throws IOException {
       Path path = rootLocation.resolve(adventurer.getPdfReference());
       Files.deleteIfExists(path);
    }

    /**
     * A method to check, if there is a file for a specific adventurer. The adventurer is identified by it's user via
     * the username and the name of the adventurer.
     *
     * @param adventurerName The name of the adventurer you want to check if a file exists for.
     * @param username The name of the user, who the adventurer belongs to.
     * @return true if there is a file, false if not.
     */
    public boolean doesFileExist(String adventurerName, String username) {
        Adventurer adventurer = adventurerService.getAdventurerByAdventurerAndUserName(adventurerName, username);
        if (adventurer.getPdfReference() == null || adventurer.getPdfReference().equals("")) {
            return false;
        }
        Path path = rootLocation.resolve(adventurer.getPdfReference());
        return Files.exists(path);
    }

    /**
     * Delete all Files currently in the upload-dir directory and the directory itself with it.
     */
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }

    /**
     * The init-method for the filesystem, which creates the upload-dir directory anew.
     */
    public void init() {
        try {
            Files.createDirectory(rootLocation);
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize storage!");
        }
    }
}
