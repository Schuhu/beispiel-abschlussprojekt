package de.awacademy.tollesprojekt.backend.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * A method to get a list of all users in the database.
     *
     * @return The list of those users.
     */
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    /**
     * A method to get a user from the database by it's username.
     *
     * @param username Said username. It should be unique.
     * @return Said user. If it returns null, there is no user with this username.
     */
    public User getUserByUsername(String username) {
        Optional<User> optionalUser = userRepository.findByUsername(username);
        return optionalUser.orElse(null);
    }

    /**
     * This method is there, so is the database is recreated, there is a user to confirm other users with, so you can
     * recreate the database content just from the web-application.
     */
    public void init() {
        if (getUsers().size() == 0) {
            User admin = new User("admin", passwordEncoder.encode("Th1s1sTheS1tuati0nTh1sP455word1sF0r"),
                    true, "visitor", true);
        }
    }

    /**
     * This method is a check, if a user of a certain username is present in the database.
     *
     * @param username The username you want to check.
     * @return True if there is one, false if there is none.
     */
    public boolean checkIfUserNamePresent(String username) {
        return userRepository.findByUsername(username).isPresent();
    }

    /**
     * This method is used to register a new user to the database.
     *
     * @param registerDTO The DTO containing all information for the new user.
     * @return The User that got saved to the database.
     */
    public User registerUser(RegisterDTO registerDTO) {
        String encodedPassword = passwordEncoder.encode(registerDTO.getPassword());
        User user = new User(registerDTO.getUsername(), encodedPassword, false
                , registerDTO.getRoleInGroup(), false);
        return userRepository.save(user);
    }

    /**
     * This method saves users to the database. It is used to either save new user or patch existing users.
     *
     * @param user The user to save. If its ID already exists you patch it, if the ID does not exist you create a new
     *             database entry.
     * @return The user  that made it to the database.
     */
    public User saveUser(User user) {
        return userRepository.save(user);
    }

    /**
     * This method is used to dele users from the database.
     *
     * @param user The user you want to delete.
     */
    public void deleteUser(User user) {
        userRepository.delete(user);
    }
}
