package de.awacademy.tollesprojekt.backend.gameTable;

import java.util.UUID;

public class SubscribeUserTableDTO {
    private String userName;
    private UUID gameTableId;

    public SubscribeUserTableDTO(String userName, UUID gameTableId) {
        this.userName = userName;
        this.gameTableId = gameTableId;
    }

    public String getUserName() {
        return userName;
    }

    public UUID getGameTableId() {
        return gameTableId;
    }
}
