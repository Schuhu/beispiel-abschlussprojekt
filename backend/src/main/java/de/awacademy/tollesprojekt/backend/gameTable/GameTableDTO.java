package de.awacademy.tollesprojekt.backend.gameTable;

import de.awacademy.tollesprojekt.backend.adventurer.AdventurerDTO;
import de.awacademy.tollesprojekt.backend.security.UserDTO;

import java.util.UUID;

public class GameTableDTO {

    private UUID id;
    private String name;
    private UserDTO owningGM;
    private AdventurerDTO[] adventurerList;
    private UserDTO[] tempSubscribedUserList;
    private UserDTO[] subscribedUserList;

    public GameTableDTO(UUID id, String name, UserDTO owningGM, AdventurerDTO[] adventurerList,
                        UserDTO[] tempSubscribedUserList, UserDTO[] subscribedUserList) {
        this.name = name;
        this.owningGM = owningGM;
        this.adventurerList = adventurerList;
        this.id = id;
        this.tempSubscribedUserList = tempSubscribedUserList;
        this.subscribedUserList = subscribedUserList;
    }

    public String getName() {
        return name;
    }

    public UserDTO getOwningGM() {
        return owningGM;
    }

    public AdventurerDTO[] getAdventurerList() {
        return adventurerList;
    }

    public UUID getId() {
        return id;
    }

    public UserDTO[] getTempSubscribedUserList() {
        return tempSubscribedUserList;
    }

    public UserDTO[] getSubscribedUserList() {
        return subscribedUserList;
    }
}
