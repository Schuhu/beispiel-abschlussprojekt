package de.awacademy.tollesprojekt.backend.adventurer;

import de.awacademy.tollesprojekt.backend.gameTable.GameTable;
import de.awacademy.tollesprojekt.backend.inventory.InventoryItem;
import de.awacademy.tollesprojekt.backend.user.User;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

/**
 * This is the Entity class for the adventurers, the representation of a player character in this system.
 * They can be organized into gametables and can have a pdf uploaded to represent them.
 *
 */

@Entity
public class Adventurer {

    @Id
    @GeneratedValue
    @Column(columnDefinition = "BINARY(16)")
    private UUID id;

    private String name;
    private String pdfReference;

    @ManyToOne
    private User user;

    @ManyToOne
    private GameTable gameTable;

    @OneToMany(mappedBy = "adventurer")
    private List<InventoryItem> inventory;


    public Adventurer() { }

    /**
     * This is the Entity class for the adventurers, Entitys represent the Database entries in Java.
     * Adventurers are the representation of a player character in this system.
     * They can be organized into gametables and can have a pdf uploaded to represent them.
     *
     * @param name the name you want the adventurer to have.
     *
     * @param pdfReference the name of any pdf document, that might be associated with this adventurer.
     *                     Null or "" for no pdf yet.
     *
     * @param user the user, who created the adventurer and is associated with this adventurer, it's owner if you will.
     */
    public Adventurer(String name, String pdfReference, User user) {
        this.name = name;
        this.pdfReference = pdfReference;
        this.user = user;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    /**
     * @return The PDF-Reference is a String with the name of the pdf in the upload-dir folder in src.
     * null or "" means there is no PDF for this adventurer.
     */
    public String getPdfReference() {
        return pdfReference;
    }

    public User getUser() {
        return user;
    }

    public List<InventoryItem> getInventory() { return inventory; }

    public void setUser(User user) { this.user = user; }

    public void setName(String name) {
        this.name = name;
    }

    public void setPdfReference(String pdfReference) {
        this.pdfReference = pdfReference;
    }

    public void setGameTable(GameTable gameTable) {
        this.gameTable = gameTable;
    }
}
