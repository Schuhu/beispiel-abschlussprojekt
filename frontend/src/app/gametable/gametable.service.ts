import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class GametableService {

  constructor(private httpClient: HttpClient) { }

  public deleteTable(owningGMName: string, gameTableName): Observable<any> {
    return this.httpClient.delete('/api/deleteTable',
      {params: {owningGM: owningGMName, gameTable: gameTableName}});
  }
}
