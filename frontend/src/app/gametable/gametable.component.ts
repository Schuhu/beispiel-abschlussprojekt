import {Component, OnInit} from '@angular/core';
import {Gametable} from '../interfaces/gametable';
import {HttpClient} from '@angular/common/http';
import {User} from '../interfaces/user';
import {SecurityService} from '../security.service';
import {AdventurerService} from '../adventurer/adventurer.service';
import {ChooseTableDTO} from '../interfaces/ChooseTableDTO';
import {GametableService} from './gametable.service';
import {FormControl, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {SubscribeUserTableDTO} from '../interfaces/SubscribeUserTableDTO';

@Component({
  selector: 'app-gametable',
  templateUrl: './gametable.component.html',
  styleUrls: ['./gametable.component.css']
})
export class GametableComponent implements OnInit {
  sessionUser: User | null = null;
  adventurerToChooseTableForName: string | null = null;
  gametableList: Gametable[] = [];
  newGametable = new FormControl('', [
    Validators.required,
    Validators.minLength(3),
    Validators.maxLength(20),
    Validators.pattern('^[a-zA-Z0-9äöüÄÖÜß ]+$'),
  ]);
  gametableErrorMessage = '';

  constructor(private httpClient: HttpClient,
              private securityService: SecurityService,
              private adventurerService: AdventurerService,
              private gameTableService: GametableService,
              private router: Router) {
  }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
    if (this.sessionUser === null) {
      this.router.navigateByUrl('/login');
      console.log('You were redirected to login, because you are not logged in');
      return;
    }
    this.adventurerService.getAdventurerToChooseTableForName().subscribe(
      s => this.adventurerToChooseTableForName = s
    );
    this.adventurerService.getAdventurerListChanged().subscribe(
      s => this.getListOfTables()
    );
  }

  getListOfTables() {
    if (this.sessionUser.roleInGroup === 'gamemaster') {
      this.getListOfTablesByGM(this.sessionUser.username);
    } else {
      this.getListOfTablesByUser(this.sessionUser.username);
    }
    // this.httpClient.get<Gametable[]>('/api/tableList').subscribe(list => this.gametableList = list);
    // console.log(this.sessionUser);
  }

  getListOfTablesByGM(username: string) {
    // So übermittelt man den benötigten Username als Parameter.
    // params sollte eine Map übegeben mit "owningGMUsername" als key und dem in die Funktion übergebenen username als parameter.
    this.httpClient.get<Gametable[]>('/api/tableListByGM',
      {params: {owningGMUsername: username}}).subscribe(
      list => {
        this.gametableList = list;
      });
  }

  getListOfTablesByUser(username: string) {
    this.httpClient.get<Gametable[]>('/api/tableListByUser',
      {params: {user: username}}).subscribe(list => {
      this.gametableList = list;
    });
  }


  postNewGameTable() {
    if (this.newGametable.valid) {
      this.gametableErrorMessage = '';
      const gametableDTO: Gametable = {
        id: null,
        name: this.newGametable.value,
        owningGM: this.sessionUser,
        adventurerList: [],
        tempSubscribedUserList: [],
        subscribedUserList: [],
      };
      this.httpClient.post('/api/newGameTable', gametableDTO).subscribe(
        () => this.getListOfTables()
      );
      this.newGametable.reset('');
    } else {
      if (this.newGametable.errors.required) {
        this.gametableErrorMessage = 'The gametable needs a name.';
      } else if (this.newGametable.errors.minlength) {
        this.gametableErrorMessage = 'The name must be at least 3 characters long.';
      } else if (this.newGametable.errors.maxlength) {
        this.gametableErrorMessage = 'The name must be at most 20 characters long.';
      } else if (this.newGametable.errors.pattern) {
        this.gametableErrorMessage = 'The name must consist of numbers, letters and spaces.';
      }
    }
  }

  patchTableForAdventurer(owningGMName: string, gametableName: string) {
    const chooseTableDTO: ChooseTableDTO = {
      sessionUserName: this.sessionUser.username,
      owningGMName,
      gametableName,
      adventurerName: this.adventurerToChooseTableForName,
    };
    this.httpClient.patch('/api/chooseTableForAdventurer', chooseTableDTO).subscribe(
      () => {
        this.getListOfTables();
        this.adventurerService.setAdventurerToChooseTableForName(null);
      }
    );
  }

  confirmSubscribedUser(user: User, gametable: Gametable) {
    const suscribeUserTableDTO: SubscribeUserTableDTO = {
      userName: user.username,
      gameTableId: gametable.id,
    };
    this.httpClient.patch('/api/subscribeUserToTable', suscribeUserTableDTO).subscribe(() => this.getListOfTables());
  }

  deleteTempSubscribedUser(user: User, gametable: Gametable) {
    this.httpClient.delete('/api/deleteTempSubscribedUserFromTable',
      {params: {userRequestName: user.username, gametableid: gametable.id.toString()}})
      .subscribe(() => this.getListOfTables());
  }

  deleteSubscribedUser(user: User, gametable: Gametable) {
    this.httpClient.delete('/api/deleteSubscribedUserFromTable',
      {params: {userRequestName: user.username, gametableid: gametable.id.toString()}})
      .subscribe(() => this.getListOfTables());
  }

  deleteTable(gametable: Gametable) {
    this.gameTableService.deleteTable(gametable.owningGM.username, gametable.name).subscribe(
      () => this.getListOfTables()
    );
  }

}
