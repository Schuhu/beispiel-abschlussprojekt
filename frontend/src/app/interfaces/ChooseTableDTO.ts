export interface ChooseTableDTO {
  sessionUserName: string;
  owningGMName: string;
  gametableName: string;
  adventurerName: string;
}
