import {v4} from 'uuid/interfaces';

export interface User {
  id: v4;
  username: string;
  adminStatus: boolean;
  roleInGroup: string;
}
