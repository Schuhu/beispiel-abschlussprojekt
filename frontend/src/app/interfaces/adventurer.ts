import {User} from './user';
import {InventoryItem} from './inventoryItem';
import {v4} from 'uuid/interfaces';

export interface Adventurer {
  id: v4;
  name: string;
  downloadLink: string;
  user: User;
  inventory: InventoryItem [];
}
