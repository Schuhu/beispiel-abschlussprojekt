import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FileSelectDirective } from 'ng2-file-upload';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SessionUserComponent } from './session-user/session-user.component';
import {HttpClientModule} from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RegisterComponent } from './register/register.component';
import { UploadComponent } from './upload/upload.component';
import { GametableComponent } from './gametable/gametable.component';
import { FormUploadComponent } from './upload/form-upload/form-upload.component';
import { AdventurerComponent } from './adventurer/adventurer.component';
import { AdminComponent } from './admin/admin.component';
import { NavbarComponent } from './navbar/navbar.component';
import { InventoryComponent } from './inventory/inventory.component';
import { HelpWindowComponent } from './help-window/help-window.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { ImprintComponent } from './imprint/imprint.component';
import { FindGametableComponent } from './gametable/find-gametable/find-gametable.component';
import { GametableProfileComponent } from './gametable/gametable-profile/gametable-profile.component';

@NgModule({
  declarations: [
    AppComponent,
    SessionUserComponent,
    LoginComponent,
    RegisterComponent,
    UploadComponent,
    FileSelectDirective,
    RegisterComponent,
    GametableComponent,
    FormUploadComponent,
    AdventurerComponent,
    AdminComponent,
    NavbarComponent,
    InventoryComponent,
    HelpWindowComponent,
    DashboardComponent,
    PrivacyPolicyComponent,
    ImprintComponent,
    FindGametableComponent,
    GametableProfileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
