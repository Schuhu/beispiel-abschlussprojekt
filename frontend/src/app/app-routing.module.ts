import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AdminComponent} from './admin/admin.component';
import {AdventurerComponent} from './adventurer/adventurer.component';
import {GametableComponent} from './gametable/gametable.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {FindGametableComponent} from './gametable/find-gametable/find-gametable.component';
import {ImprintComponent} from './imprint/imprint.component';
import {PrivacyPolicyComponent} from './privacy-policy/privacy-policy.component';


const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'admin', component: AdminComponent},
  {path: 'adventurers', component: AdventurerComponent},
  {path: 'gametables', component: GametableComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'imprint', component: ImprintComponent},
  {path: 'privacypolicy', component: PrivacyPolicyComponent},
  {path: 'findgametable', component: FindGametableComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
