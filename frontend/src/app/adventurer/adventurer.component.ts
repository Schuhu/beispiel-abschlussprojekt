import {Component, OnInit} from '@angular/core';
import {User} from '../interfaces/user';
import {HttpClient} from '@angular/common/http';
import {SecurityService} from '../security.service';
import {Adventurer} from '../interfaces/adventurer';
import {AdventurerService} from './adventurer.service';
import {InventoryItem} from '../interfaces/inventoryItem';
import {UploadService} from '../upload/upload.service';
import {InventoryService} from '../inventory/inventory.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-adventurer',
  templateUrl: './adventurer.component.html',
  styleUrls: ['./adventurer.component.css']
})
export class AdventurerComponent implements OnInit {
  newAdventurer = new FormControl('', [
    Validators.required,
    Validators.minLength(3),
    Validators.maxLength(20),
    Validators.pattern('^[a-zA-Z0-9äöüÄÖÜß ]+$'),
  ]);

  newItem = new FormGroup({
    itemName: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(20),
      Validators.pattern('^[a-zA-Z0-9äöüÄÖÜß ]+$'),
    ]),
    itemDescription: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(200),
      Validators.pattern('^[a-zA-Z0-9äöüÄÖÜß.,!? ]+$'),
    ]),
    amount: new FormControl(0, [
      Validators.required,
      Validators.min(0),
      Validators.max(1000000)
    ]),
  });
  adventurerErrorMessage = '';
  sessionUser: User | null = null;
  adventurerToUploadToName: string | null = null;
  adventurerToChooseTableForName: string | null = null;
  adventurerToShowInventoryForName: string | null = null;
  adventurers: Adventurer[];
  errorMessage = '';
  itemNameError = '';
  itemDescriptionError = '';
  itemAmountError = '';

  constructor(private httpClient: HttpClient, private securityService: SecurityService,
              private adventurerService: AdventurerService, private uploadService: UploadService,
              private inventoryService: InventoryService, private router: Router) {
  }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => {
        this.sessionUser = u;
        this.getAdventurerListOfUser();
      }
    );
    if (this.sessionUser === null) {
      this.router.navigateByUrl('/login');
      console.log('You were redirected to login, because you are not logged in');
      return;
    }
    this.getAdventurerListOfUser();
    this.adventurerService.getAdventurerToUploadToName().subscribe(
      s => this.adventurerToUploadToName = s
    );
    this.adventurerService.getAdventurerToChooseTableForName().subscribe(
      s => this.adventurerToChooseTableForName = s
    );
    this.adventurerService.getAdventurerListChanged().subscribe(
      s => {
        this.getAdventurerListOfUser();
      }
    );
    this.adventurerService.getUploadComplete().subscribe(
      s => {
        this.getAdventurerListOfUser();
      },
      () => {
        this.errorMessage = 'FileUpload failed!';
      }
    );
  }

  setAdventurerToUploadTo(adventurername: string) {
    if (this.adventurerToUploadToName === adventurername) {
      this.adventurerService.setAdventurerToUploadToName(null);
    } else {
      this.adventurerService.setAdventurerToUploadToName(adventurername);
    }
  }

  setAdventurerToChooseTableFor(adventurername: string) {
    if (this.adventurerToChooseTableForName === adventurername) {
      this.adventurerService.setAdventurerToChooseTableForName(null);
    } else {
      this.adventurerService.setAdventurerToChooseTableForName(adventurername);
    }
  }

  setAdventurerToShowInventoryFor(adventurername: string) {
    if (this.adventurerToShowInventoryForName === adventurername) {
      this.adventurerToShowInventoryForName = null;
    } else {
      this.adventurerToShowInventoryForName = adventurername;
    }
  }

  postNewAdventurer() {
    if (this.newAdventurer.valid) {
      this.adventurerErrorMessage = '';
      const adventurerDTO: Adventurer = {
        id: null,
        name: this.newAdventurer.value,
        downloadLink: null,
        user: this.sessionUser,
        // TODO inventory is null
        inventory: null
      };
      this.httpClient.post('/api/newAdventurer', adventurerDTO).subscribe(
        () => {
          this.adventurerService.setAdventurerListChanged('update pls.');
        }
      );
      this.newAdventurer.reset('');
    } else {
      if (this.newAdventurer.errors.required) {
        this.adventurerErrorMessage = 'The adventurer needs a name.';
      } else if (this.newAdventurer.errors.minlength) {
        this.adventurerErrorMessage = 'The name must be at least 3 characters long.';
      } else if (this.newAdventurer.errors.maxlength) {
        this.adventurerErrorMessage = 'The name must be at most 20 characters long.';
      } else if (this.newAdventurer.errors.pattern) {
        this.adventurerErrorMessage = 'The name must consist of numbers, letters and spaces.';
      }
    }
  }

  getAdventurerListOfUser() {
    if (this.sessionUser !== null) {
      this.adventurerService.getAdventurerListForUser(this.sessionUser).subscribe(
        list => this.adventurers = list
      );
    }
  }

  deleteAdventurer(adventurer: Adventurer) {
    this.adventurerService.deleteAdventurer(adventurer).subscribe(
      () => {
        this.adventurerService.setAdventurerListChanged('update pls.');
      }
    );
  }

  deleteItem(item: InventoryItem, adventurer: Adventurer) {
    this.inventoryService.deleteInventoryItem(item, adventurer).subscribe(
      () => this.getAdventurerListOfUser()
    );
  }

  addItem() {
    if (this.newItem.valid) {
      this.itemNameError = '';
      this.itemDescriptionError = '';
      this.itemAmountError = '';
      this.inventoryService.addItem({
          id: null,
          name: this.newItem.get('itemName').value,
          description: this.newItem.get('itemDescription').value,
          amount: this.newItem.get('amount').value,
        },
        this.adventurerToShowInventoryForName,
        this.sessionUser.username).subscribe(() =>
        this.getAdventurerListOfUser()
      );
      this.newItem.get('itemName').reset('');
      this.newItem.get('itemDescription').reset('');
      this.newItem.get('amount').reset(0);
    } else {
      if (this.newItem.get('itemName').errors !== null) {
        if (this.newItem.get('itemName').errors.required) {
          this.itemNameError = 'Name is required!';
        } else if (this.newItem.get('itemName').errors.minlength) {
          this.itemNameError = 'Name must contain between 3 and 20 characters!';
        } else if (this.newItem.get('itemName').errors.maxlength) {
          this.itemNameError = 'Name must contain between 3 and 20 characters!';
        } else if (this.newItem.get('itemName').errors.pattern) {
          this.itemNameError = 'Name can only consist of numbers, letters and spaces.';
        }
      } else {
        this.itemNameError = '';
      }
      if (this.newItem.get('itemDescription').errors !== null) {
        if (this.newItem.get('itemDescription').errors.minlength) {
          this.itemDescriptionError = 'Description must be between 3 and 200 characters!';
        } else if (this.newItem.get('itemDescription').errors.maxlength) {
          this.itemDescriptionError = 'Description must be between 3 and 200 characters!';
        } else if (this.newItem.get('itemDescription').errors.required) {
          this.itemDescriptionError = 'Description is required!';
        } else if (this.newItem.get('itemDescription').errors.pattern) {
          this.itemDescriptionError = 'Only letters, numbers and certain punctuations are allowed.';
        }
      } else {
        this.itemDescriptionError = '';
      }

      if (this.newItem.get('amount').errors !== null) {
        if (this.newItem.get('amount').errors.minvalue) {
          this.itemAmountError = 'Amount must be zero or bigger';
        }
      } else {
        this.itemAmountError = '';
      }
    }

  }

  changeAmountOfItem(item: InventoryItem) {
    this.inventoryService.changeAmount(item, this.adventurerToShowInventoryForName,
      this.sessionUser.username).subscribe(
      () => this.getAdventurerListOfUser()
    );
  }

  incrementAmount(item: InventoryItem) {
    const itemToChange: InventoryItem = {
      id: item.id,
      name: item.name,
      description: item.description,
      amount: item.amount,
    };
    itemToChange.amount += 1;
    this.changeAmountOfItem(itemToChange);
  }

  decrementAmount(item: InventoryItem) {
    const itemToChange: InventoryItem = {
      id: item.id,
      name: item.name,
      description: item.description,
      amount: item.amount,
    };
    itemToChange.amount -= 1;
    this.changeAmountOfItem(itemToChange);
  }


  deleteErrorMessage() {
    this.errorMessage = '';
  }
}
