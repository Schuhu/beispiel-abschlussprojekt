import { Component, OnInit } from '@angular/core';
import {User} from '../interfaces/user';
import {Router} from '@angular/router';
import {SecurityService} from '../security.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  sessionUser: User|null = null;

  constructor(private router: Router, private securityService: SecurityService) { }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
    if (this.sessionUser === null) {
      this.router.navigateByUrl('/login');
      console.log('You were redirected to login, because you are not logged in');
      return;
    }
  }

}
