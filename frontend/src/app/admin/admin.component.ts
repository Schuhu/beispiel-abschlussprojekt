import {Component, OnInit} from '@angular/core';
import {User} from '../interfaces/user';
import {SecurityService} from '../security.service';
import {HttpClient} from '@angular/common/http';
import {AdventurerService} from '../adventurer/adventurer.service';
import {Adventurer} from '../interfaces/adventurer';
import {Router} from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  sessionUser: User | null;
  userList: User[];
  unconfirmedUserList: User[];
  adventurers: Adventurer[];

  constructor(private httpClient: HttpClient, private securityService: SecurityService,
              private adventurerService: AdventurerService, private router: Router) {
  }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => {
        this.sessionUser = u;
        this.getListofUsers();
      }
    );
    if (this.sessionUser === null) {
      this.router.navigateByUrl('/login');
      console.log('You have no right being here!');
      return;
    }
    this.adventurerService.getAdventurerListChanged().subscribe(
      s => this.getAdventurerList()
    );
    this.getListofUsers();
    this.getListofUnconfirmedUsers();
    this.getAdventurerList();
  }

  getListofUsers() {
    this.httpClient.get<User[]>('/api/userList').subscribe(list => this.userList = list);
  }

  getListofUnconfirmedUsers() {
    this.httpClient.get<User[]>('/api/userListUnconfirmed').subscribe(list => this.unconfirmedUserList = list);
  }

  patchChangeAdminStatus(user: User) {
    this.httpClient.patch('/api/nominateAdmin', user).subscribe(
      () => this.getListofUsers()
    );
  }

  getAdventurerList() {
    this.adventurerService.getAdventurerList().subscribe(list => this.adventurers = list);
  }

  deleteAdventurer(adventurer: Adventurer) {
    this.adventurerService.deleteAdventurer(adventurer).subscribe(
      () => this.adventurerService.setAdventurerListChanged('update pls.')

  );
  }

  deleteUser(user: User) {
    this.securityService.deleteUser(user.username).subscribe(
      () => {
        this.getListofUsers();
        this.adventurerService.setAdventurerListChanged('update pls.');
      }
    );
  }

  confirmUser(user: User) {
    this.securityService.confirmUser(user).subscribe(
      () => {
        this.getListofUnconfirmedUsers();
        this.getListofUsers();
      }
    );
  }
}
