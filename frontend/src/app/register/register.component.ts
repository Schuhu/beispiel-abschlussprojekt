import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {User} from '../interfaces/user';
import {SecurityService} from '../security.service';
import {RoleInGroupValidator} from './register.validation';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  sessionUser: User | null = null;
  registerData = new FormGroup({
    username: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(20),
      Validators.pattern('^[a-zA-Z0-9äöüÄÖÜß ]+$'),
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(20),
      Validators.pattern('^[a-zA-Z0-9äöüÄÖÜß]+$'),
    ]),
    roleInGroup: new FormControl('', [
      Validators.required,
      RoleInGroupValidator.roleInGroupShouldBeValid,
    ]),
  });
  usernameErrorMassage = '';
  passwordErrorMassage = '';
  roleInGroupErrorMassage = '';
  notificationMessage = '';

  constructor(private securityService: SecurityService) {
  }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
  }

  deleteNotificationMessage() {
    this.notificationMessage = '';
  }

  register() {
    if (this.registerData.valid) {
      this.usernameErrorMassage = '';
      this.passwordErrorMassage = '';
      this.roleInGroupErrorMassage = '';
      this.securityService.register(this.registerData.get('username').value, this.registerData.get('password').value
        , this.registerData.get('roleInGroup').value).subscribe(user => {
        if (user !== null) {
          this.notificationMessage = (user.username + ' is registered.');
        } else {
          this.notificationMessage = ('Something went wrong. :( You are probably not registered.');
        }
      }, () => this.notificationMessage = ('An error occurred during Registration, maybe the username is already taken.'));
      this.registerData.reset('');
    } else {
      if (this.registerData.get('username').errors !== null) {
        if (this.registerData.get('username').errors.required) {
          this.usernameErrorMassage = 'Username is required.';
        } else if (this.registerData.get('username').errors.minlength) {
          this.usernameErrorMassage = 'Username must be at least 3 characters long.';
        } else if (this.registerData.get('username').errors.maxlength) {
          this.usernameErrorMassage = 'Username must be at most 20 characters long.';
        } else if (this.registerData.get('username').errors.pattern) {
          this.usernameErrorMassage = 'Username must consist of numbers, letters and spaces.';
        }
      } else {
        this.usernameErrorMassage = '';
      }
      if (this.registerData.get('password').errors !== null) {
        if (this.registerData.get('password').errors.required) {
          this.passwordErrorMassage = 'Password is required.';
        } else if (this.registerData.get('password').errors.minlength) {
          this.passwordErrorMassage = 'Password must be at least 4 characters long.';
        } else if (this.registerData.get('password').errors.maxlength) {
          this.passwordErrorMassage = 'Password must be at most 20 characters long.';
        } else if (this.registerData.get('password').errors.pattern) {
          this.passwordErrorMassage = 'Password must consist of numbers and letters.';
        }
      } else {
        this.passwordErrorMassage = '';
      }
      if (this.registerData.get('roleInGroup').errors !== null) {
        if (this.registerData.get('roleInGroup').errors.required) {
          this.roleInGroupErrorMassage = 'The role is required.';
        } else if (this.registerData.get('roleInGroup').errors.shouldBeOneOfTheOptions) {
          this.roleInGroupErrorMassage = 'Please choose one of the prepared options.';
        }
      } else {
        this.roleInGroupErrorMassage = '';
      }
    }
  }
}
