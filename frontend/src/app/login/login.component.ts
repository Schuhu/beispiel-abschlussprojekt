import {Component, OnInit} from '@angular/core';
import {User} from '../interfaces/user';
import {SecurityService} from '../security.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  sessionUser: User | null = null;
  notificationMessage = '';
  prevention: boolean;
  loginData = {
    username: '', password: ''
  };

  constructor(private securityService: SecurityService) {
  }

  ngOnInit() {
    this.prevention = true;
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
    this.securityService.getLogInErrorPrevention().subscribe(
      s => this.prevention = true
    );
    this.securityService.getLogInError().subscribe(
      s => {
        if (this.prevention) {
          this.prevention = false;
        } else {
          this.notificationMessage = s;
        }
      }
    );
  }

  deleteNotificationMessage() {
    this.notificationMessage = '';
  }

  login() {
    this.securityService.login(this.loginData.username, this.loginData.password);
  }
}
